<?php
require_once('../includes/config.inc.php');
require_once(MODEL_PATH . 'Activity.php');

header('Content-Type: application/json');

switch ($_SERVER['REQUEST_METHOD']) 
{
  case 'GET':    
    if(!count($_GET))
    {
      Responser::JSON(
        Activity::getAll()
      );
    }
    else if(isset($_GET['activityId']) && Validator::isInt($_GET['activityId'])) 
    {
      try {
        $activity = new Activity($_GET['activityId']);
        
        Responser::text($activity->toJSON());
      } catch (Exception $ex) {
        Responser::error($ex);
      }
    }
    else
    {
      Responser::badRequest();
    }

    break;

  case 'POST':
    $body = json_decode(file_get_contents('php://input'));

    if($body === null)
    {
      Responser::error('Not body', true);
    }

    if(!count($_GET)) 
    {
      try {
        $activity = new Activity($body);

        $activity->create();

        Responser::text($activity->toJSON(), HTTPCodes::CREATED);

      } catch (Exception $ex) {
        Responser::error($ex);
      }
    }
    else
    {
      Responser::badRequest();
    }

    break;

  case 'PUT':
    $body = json_decode(file_get_contents('php://input'));

    if($body === null)
    {
      Responser::error('Not body', true);
    }

    if(isset($_GET['activityId']) && Validator::isInt($_GET['activityId'])) 
    {
      try {
        // If that activity does not exist, it will throw an exception
        $activity = new Activity($_GET['activityId']);

        // Validate the content
        $activityToUpdate = new Activity($body);
        $activityToUpdate->setActivityId($activity->getActivityId());

        $activityToUpdate->update();

        Responser::noContent();

      } catch (Exception $ex) {        
        Responser::error($ex);
      }
    }
    else
    {
      Responser::badRequest();
    }

    break;

  case 'DELETE':
    if(isset($_GET['activityId']) && Validator::isInt($_GET['activityId'])) 
    {
      try {
        $activity = new Activity($_GET['activityId']);

        $activity->delete();

        Responser::noContent();

      } catch (Exception $ex) {
        Responser::error($ex);
      }
    }
    else
    {
      Responser::badRequest();
    }

    break;
    
  default:
    Responser::methodNotAllowed();
    
    break;
}
?>