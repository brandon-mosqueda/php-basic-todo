<?php  
class Math
{
  public static function round(
                           $num, 
                           int $decimals = 2
                         ) 
  {
    if(!is_numeric($num))
    {
      throw new Exception("Invalid num for rounding", HTTPCodes::BAD_REQUEST);
    }

    return floatval(bcdiv($num, '1', $decimals));
  }
}
?>