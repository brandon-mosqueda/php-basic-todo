<?php
class DatabaseException extends Exception
{
  public function __construct($message, $code = 0, Exception $previous = null)
  {
    parent::__construct($message, $code, $previous);  
  }

  public function __toString() 
  {
    return __CLASS__ . ": [{$this->code}]: {$this->message}";
  }
}

class MySQLWrapper
{
  protected static $connection = null;
  protected static $isTransaction = false;

  /**
   * Parameterized queries to database
   * @param  [string] $sql      The SQL query. It have to come with the same
   *                            convention of mysqli's function bind_param, 
   *                            that is to say, with = ?in the paramethers you
   *                            want to bind. e.g.
   *                              UPDATE users SET name = ?
   * @param  [string] $rules    The rules of bind_param which indicate the type
   *                            of each paramether, e.g. for a string param:
   *                            's'
   * @param  [array]  $params   The query arguments in the same order as the 
   *                            SQL query and rules.
   * @param  [bool]   $isSelect If true, returns all the elements of the query,
   *                            otherwise returns an object with the query
   *                            information as result, affected_rows and 
   *                            insert_id.
   * @throws DatabaseException  When there is an error with the SQL statement or
   *                            with the databse itself.
   * @return [mysqli_stmt] 
   */
  protected static function query(
                              string $sql, 
                              string $rules = null, 
                              array $params = null,
                              bool $isSelect = false
                            )
  {
    $database = self::getConnection();
    $statement = $database->stmt_init();
    $result = new stdClass();
    $error = null;

    if($statement->prepare($sql)) 
    {
      if($rules !== null && $params !== null)
      {
        $statement->bind_param($rules, ...$params);
      }

      $executeResult = $statement->execute();
      
      if($isSelect)
      {
        $result = $statement->get_result();
      }
      else
      {
        $result->result = $executeResult;
        $result->affected_rows = $statement->affected_rows;
        $result->insert_id = $statement->insert_id;
      }

      if($statement->error)
      {
        $error = $statement->error;
      }

      $statement->close();
    }

    if($error === null && $database->error)
    {
      $error = $database->error;
    }

    if(!static::$isTransaction)
    {
      $database->close();
      static::$connection = null;
    }

    if($error !== null)
    {
      throw new DatabaseException($error, HTTPCodes::BAD_REQUEST);
    }

    return $result;
  }

  protected static function getConnection()
  {
    if(static::$connection === null)
    {
      static::$connection = new Database();
    }

    return static::$connection;
  }

  public static function selectBySql(
                           string $sql, 
                           string $rules = null, 
                           array $params = null
                         )
  {
    return self::query($sql, $rules, $params, true);
  }

  public static function getArrayBySql(
                           string $sql, 
                           string $rules = null, 
                           array $params = null
                         ) 
  {
    $result = self::selectBySql($sql, $rules, $params);
  
    if(!$result)
    {
      return [];
    }

    $array = array();

    while($object = $result->fetch_object())
    {
      $array[] = $object;
    }

    return $array;
  }

  public static function getSingleArrayBySql(
                           string $sql, 
                           string $field, 
                           string $rules = null, 
                           array $params = null
                         ) 
  {
    $result = self::selectBySql($sql, $rules, $params);
  
    if(!$result)
    {
      return [];
    }

    $array = array();
  
    while ($object = $result->fetch_object())
    {
      $array[] = $object->$field;
    }

    return $array;
  }

  public static function getObjectBySql(
                           string $sql, 
                           string $rules = null, 
                           array $params = null
                         ) 
  {
    $result = self::selectBySql($sql, $rules, $params);
  
    if(!$result)
    {
      return null;
    }

    return $result->fetch_object();
  }

  public static function getCountBySql(
                           string $sql, 
                           string $rules = null, 
                           array $params = null
                         ) 
  {
    $result = self::selectBySql($sql, $rules, $params);
  
    if(!$result)
    {
      return 0;
    }

    return intval($result->fetch_row()[ 0 ]);
  }

  public static function startTransaction()
  {
    $database = self::getConnection();
    static::$isTransaction = true;

    return $database->begin_transaction();
  }

  public static function rollbackTransaction()
  {
    $database = self::getConnection();
    static::$isTransaction = false;

    $result = $database->rollback();
    $database->close();

    static::$connection = null;

    return $result;
  }

  public static function commitTransaction()
  {
    $database = self::getConnection();
    static::$isTransaction = false;

    $result = $database->commit();
    $database->close();

    static::$connection = null;

    return $result;
  }
}
?>