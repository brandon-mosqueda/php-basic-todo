<?php  
require_once(LIB_PATH . 'Gump.php');

class ValidationException extends Exception
{
  public function __construct($message, $code = 0, Exception $previous = null)
  {
    parent::__construct($message, $code, $previous);  
  }

  public function __toString() 
  {
    return __CLASS__ . ": [{$this->code}]: {$this->message}";
  }
}

class Validator
{
  private $rules;
  private $hasNewOwnValidationRule;

  public function __construct(array $rules) 
  {
    $this->rules = $rules;
    $hasNewOwnValidationRule = false;
  }

  public function validateOneField($value, $field) 
  {
    self::validate([ $field => $value ], [ $field ]);
  }

  public function validate($values, $fields = false) 
  {
    $values = self::castIfObjectToArray($values);

    if($fields === false)
      $fields = self::getKeysFromRules();

    $gump = new GUMP();
    
    $gump->sanitize($values);
    $validation = [];

    foreach ($fields as $field)
      $validation[ $field ] = $this->rules[ $field ];

    $gump->validation_rules($validation);

    if(!$gump->run($values)) 
    {
      // If it has new own validation, there is no error message on gump file
      // and it produces an error when calling get_readable_errors function.
      $errors = $gump->get_readable_errors(true);

      throw new ValidationException($errors, 400);
    }
  }

  private function castIfObjectToArray($params) 
  {
    return gettype($params) === 'object' ?
           (array) $params :
           $params;
  }

  private function getKeysFromRules() 
  {
    $keys = [];

    foreach ($this->rules as $key => $value) 
      $keys[] = $key;

    return $keys;
  }

  public function addOrReplaceValidationRule($ruleName, $ruleOptions)
  {
    $this->rules[ $ruleName ] = $ruleOptions;
  }

  public function addNewIsValidOptionFromArray(
                    string $ruleName,
                    array $validValues,
                    string $errorMessage = 'El campo {field} no es válido'
                  )
  {
    $this->hasNewOwnValidationRule = true;

    try {
      GUMP::add_validator(
        $ruleName,
        function(string $field, array $input, $param = null) use ($validValues) {
          return in_array($input[ $field ], $validValues);
        },
        $errorMessage
      );
    } catch (Exception $ex) {
      
    }
  }

  public static function isInt($val)
  {
    return is_numeric($val) 
           && ($val / intval($val) == 1);
  }

  public static function isValidFile(
                           array $file, 
                           int $maxSize,
                           string $validType
                         ) 
  {
    if(
      ! $file['size']
      || $file['size'] / 1024 / 1024 > $maxSize
    )
    {
      return false;
    }

    $fileType = mime_content_type($file['tmp_name']);

    return strpos($fileType, $validType) !== false;
  }

  public static function isValidImage(array $file)
  {
    return self::isValidFile(
      $file,
      MAX_FILE_MB_SIZE,
      'image/'
    );
  }
}
?>