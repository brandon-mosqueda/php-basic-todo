<?php
class Responser
{
  public static function badRequest(bool $finishConnection = false)
  {
    return self::error('Not valid request', HTTPCodes::BAD_REQUEST, $finishConnection);
  }

  public static function notFound(bool $finishConnection = false)
  {
    return self::error('Resource not found', HTTPCodes::NOT_FOUND, $finishConnection);
  }

  public static function forbidden(bool $finishConnection = false)
  {
    return self::error('You are not allowed in this section', HTTPCodes::FORBIDDEN, $finishConnection);
  }

  public static function methodNotAllowed(bool $finishConnection = false)
  {
    return self::error('Method not allowed', HTTPCodes::METHOD_NOT_ALLOWED, $finishConnection);
  }

  public static function serverError(bool $finishConnection = false)
  {
    return self::error('There was an error in the server', HTTPCodes::SERVER_ERROR, $finishConnection);
  }

  public static function noContent(bool $finishConnection = false)
  {
    return self::response('', HTTPCodes::NO_CONTENT, $finishConnection);
  }

  public static function created(bool $finishConnection = false)
  {
    return self::response('', HTTPCodes::CREATED, $finishConnection);
  }
  
  public static function error($message, $code = null, $finishConnection = false)
  {
    if($message instanceof Exception)
    {
      $finishConnection = gettype($code) === 'boolean' ? $code : false;

      self::JSON(
        ["error" => $message->getMessage()], 
        $message->getCode(),
        $finishConnection
      );
    }
    else
    {
      if($code === null)
      {
        $code = HTTPCodes::BAD_REQUEST;
      }

      if(gettype($code) === 'boolean')
      {
        $finishConnection = $code;
        $code = HTTPCodes::BAD_REQUEST;
      }

      self::JSON(
        ["error" => $message], 
        $code,
        $finishConnection
      );
    }
  }

  public static function JSON($data, $code = null, $finishConnection = false)
  {
    if(gettype($code) === 'boolean')
    {
      $finishConnection = $code;
      $code = HTTPCodes::SUCCESS;
    }
    else if($code === null)
    {
      $code = HTTPCodes::SUCCESS;
    }

    self::response(json_encode($data), $code, $finishConnection);
  }

  public static function code(int $code, bool $finishConnection = false)
  {
    return self::text('', $code, $finishConnection);
  }

  public static function text($data, $code = null, $finishConnection = false)
  {
    if(gettype($code) === 'boolean')
    {
      $finishConnection = $code;
      $code = HTTPCodes::SUCCESS;
    }
    else if($code === null)
    {
      $code = HTTPCodes::SUCCESS;
    }

    self::response($data, $code, $finishConnection);
  }

  private static function response(
                            string $data,
                            int $code = null, 
                            bool $finishConnection = false
                          )
  {
    if($code === null)
    {
      $code = HTTPCodes::SUCCESS;
    }

    echo $data;

    http_response_code($code);

    if($finishConnection)
    {
      die();
    }
  }
}
?>