<?php
require_once(UTILS_PATH . 'Validator.php');

abstract class Model extends MySQLWrapper
{
  protected $validator;

  public static function getAll() 
  {
    return self::getArrayBySql("SELECT * FROM " . static::getTableName());
  }

  protected static function getAllByTable($table) 
  {
    return self::getArrayBySql("SELECT * FROM " . $table);
  }

  public static function getById(int $id) 
  {
    return self::getObjectBySql(
      "SELECT * FROM " . static::getTableName() . 
      " WHERE " . static::getPrimaryKeyName() . " = ?",
      'i',
      [ $id ]
    );
  }

  public static function deleteById(int $id)
  {
    return self::query( 
      "DELETE FROM " . static::getTableName() . 
      " WHERE " . static::getPrimaryKeyName() . " = ?", 
      'i', 
      [ $id ] 
    );
  }

  public function delete()
  {
    return self::deleteById($this->{static::getPrimaryKeyName()})->result;
  }

  public function toJSON(array $fields = null) 
  {
    $map = new stdClass();

    if($fields === null)
    {
      foreach ($this as $key => $value)
      {
        $map->$key = $value;
      }
    } 
    else 
    {
      foreach ($fields as $key)
      {
        $map->$key = $this->$key;
      }
    }

    unset($map->validator);

    return json_encode($map);
  }

  public function save()
  {
    return ( 
      isset($this->{static::getPrimaryKeyName()}) 
      && $this->{static::getPrimaryKeyName()} 
    ) ?
     $this->update() : 
     $this->create();
  }

  protected function validate($values, array $fields) 
  {
    return $this->validator->validate($values, $fields);
  }

  protected function validateOneField($value, $field) 
  {
    return $this->validator->validateOneField($field, $value);
  }

  protected abstract static function getTableName();

  protected abstract static function getPrimaryKeyName();
}
?>