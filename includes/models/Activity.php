<?php
class Activity extends Model
{
  protected $activityId;
  protected $title;
  protected $comment;
  protected $isCompleted;
  protected $createdDate;
  protected $finishedDate;
  
  public function __construct($data) 
  {
    $this->validator = new Validator([
      'activityId' => 'required|integer',
      'title' => 'required|max_len,250',
      'comment' => 'max_len,1000',
      'isCompleted' => 'boolean'
    ]);

    if(Validator::isInt($data)) 
    {
      $data = self::getById($data);
    }
    else if(gettype($data) === 'object') 
    {
      self::validate(
        $data,
        [
          'title',
          'comment',
          'isCompleted'
        ]
      );

      $data->activityId = null;
    }
    else
    {
      throw new Exception('Activity\'s constructor, incorrect params', 400);
    }

    if($data !== null) 
    {
      foreach ($data as $field => $value)
      {
        $this->$field = $value;
      }
    }
    else
    {
      throw new Exception('Activity not found', 404);
    }
  }
  
  public function create()
  {
    $sql = "INSERT INTO activities(
              title,
              comment
            ) VALUES (?, ?)";
    
    $queryResult = self::query(
      $sql, 
      'ss', 
      [
        $this->title,
        $this->comment
      ]
    );

    $this->activityId = $queryResult->insert_id;

    return true;
  }

  public function update()
  {
    $this->finishedDate = NULL;

    if($this->isCompleted)
    {
      $this->finishedDate = 'CURRENT_TIMESTAMP';
    }

    $sql = "UPDATE activities  
            SET title = ?,
                comment = ?,
                isCompleted = ?,
                finishedDate = {$this->finishedDate}
            WHERE activityId = ?";
    
    $queryResult = self::query(
      $sql, 
      'ssii', 
      [
        $this->title,
        $this->comment,
        intval($this->isCompleted),
        $this->activityId
      ]
    )->result;
  }

  public function getActivityId(){ return $this->activityId; }

  public function getTitle(){ return $this->title; }

  public function getComment(){ return $this->comment; }

  public function getIsCompleted(){ return $this->isCompleted; }

  public function getCreatedDate(){ return $this->createdDate; }

  public function getFinishedDate(){ return $this->finishedDate; }

  public function setActivityId(int $activityId)
  {
    self::validateOneField('activityId', $activityId);
    $this->activityId = $activityId;
  }

  public function setTitle(string $title)
  {
    self::validateOneField('title', $title);
    $this->title = $title;
  }

  public function setComment(string $comment)
  {
    self::validateOneField('comment', $comment);
    $this->comment = $comment;
  }

  public function setIsCompleted(bool $isCompleted)
  {
    self::validateOneField('isCompleted', $isCompleted);
    $this->isCompleted = $isCompleted;
  }
  
  protected static function getTableName() { return 'activities'; }
  
  protected static function getPrimaryKeyName() { return 'activityId'; }
}
?>