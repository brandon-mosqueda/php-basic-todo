<?php
session_start();

header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('MST');
setlocale(LC_MONETARY, 'es_ES');

define('DS', DIRECTORY_SEPARATOR);

define('SITE_ROOT', dirname(dirname(__FILE__)) . DS);

define('INCLUDE_PATH', SITE_ROOT . 'includes' . DS);

define('LIB_PATH', INCLUDE_PATH . 'libraries' . DS);

define('MODEL_PATH', INCLUDE_PATH . 'models' . DS);

define('UTILS_PATH', INCLUDE_PATH . 'utils' . DS);

define('VIEW_PATH', INCLUDE_PATH . 'views' . DS);

$SERVER = json_decode(
            file_get_contents(INCLUDE_PATH . 'server.json')
          )->development;

define('URL', $SERVER->url);

define('DATABASE_HOST', $SERVER->host);

define('DATABASE_NAME', $SERVER->database);

define('DATABASE_USER', $SERVER->user);

define('DATABASE_PASSWORD', $SERVER->password);

unset($SERVER);

require(UTILS_PATH . 'HTTPCodes.php');
require(UTILS_PATH . 'Responser.php');
require(UTILS_PATH . 'Database.php');
require(UTILS_PATH . 'MySQLWrapper.php');
require(UTILS_PATH . 'Math.php');

require(MODEL_PATH . 'Model.php');
?>