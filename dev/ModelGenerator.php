<?php
class ModelGenerator
{
  public static $TAB_SIZE = 2;
  private $TAB;

  private $modelName;
  private $props;
  private $primaryKey;

  public function __construct(
           string $modelName, array $props, string $primaryKey
         )
  {
    $this->TAB = $this->doTab();

    $this->modelName = $modelName;
    $this->props = $props;
    $this->primaryKey = $primaryKey;
  }

  private function doTab()
  {
    return $this->repeatText(' ', static::$TAB_SIZE);
  }

  private function getTabs($amount)
  {
    return $this->repeatText($this->TAB, $amount);
  }

  private function repeatText(string $text, int $times) 
  {
    $finalText = '';

    for ($i = 0; $i < $times; $i++) 
    {
      $finalText .= $text;
    }

    return $finalText;
  }

  private function toTitleCase($text) 
  {
    $text = strtolower($text);
    $text[ 0 ] = strtoupper($text[ 0 ]);

    return $text;
  }

  private function getRules($type)
  {
    switch ($type) 
    {
      case 'i':
        return "required|integer";
      case 'b':
        return "required|boolean";
      case 'd':
        return "required|numeric";
      default:
        return "required|max_len,100|min_len,10";
    }
  }

  private function letterTypeToType($letter)
  {
    switch($letter) 
    {
      case 'i':
        return "int";
      case 'b':
        return "bool";
      case 'd':
        return "float";
      default:
        return "string";
    } 
  }

  private function getSetName($name) 
  {
    $new = strtoupper($name[0]);

    for ($i = 1; $i < strlen($name); $i++) 
    {
      if($name[ $i ] !== '_')
      {
        $new .= $name[ $i ];
      }
      else
      {
        $new .= strtoupper($name[ ++$i ]);
      }
    }

    return $new;
  }

  public function generateModel()
  {
    //private $id;
    $propsOfClass = $this->generatePropsOfClass();

    //'id' => 'required|numeric',
    $propsValidationRules = $this->generatePropsValidationRules();

    //'name', 'email'
    $stringArray = $this->generateFieldsAsStringsArray();

    //name, email, para el query de insert
    $createFields = $this->generateFieldsAsStringsArrayForCreateStament();

    //Interrogation ?, to create and update method
    $interrogation = $this->generateInterrogationMarksForCreateStatement();

    //Bind params 'sssiiss'
    $bindParamsLetters = $this->generateBindParamLetters();
    $paramsForCreateStatement = $this->generateParamsForCreateStatement();

    //To update method SET name = ?
    $setParams = $this->generateSetParams();

    //public function getName() { return \$this->name; }
    $getters = $this->generateGetters();

    // public function setName(\$name) 
    // {
    //   self::validateOneField('name', \$name);
    //   \$this->name = \$name;
    // }
    $setters = $this->generateSetters();

    $modelNameInLowerCase = strtolower($this->modelName);
    $modelNameInTitleCase = $this->toTitleCase($this->modelName);

      $result = "<?php
class " . $modelNameInTitleCase . " extends Model
{
  " . $propsOfClass . "
  public function __construct(\$data) 
  {
    \$this->validator = new Validator([
      " . $propsValidationRules . "
    ]);

    if(is_numeric(\$data)) 
    {
      \$data = self::getById(\$data);
    }
    else if(gettype(\$data) === 'object') 
    {
      self::validate(
        \$data,
        [
          " . $stringArray . "
        ]
      );

      \$data->" . $this->primaryKey . " = false;
    }
    else
    {
      throw new Exception('" . $modelNameInTitleCase . "\\'s constructor, incorrect params', 400);
    }

    if(\$data !== null) 
    {
      foreach (\$data as \$field => \$value)
      {
        \$this->\$field = \$value;
      }
    }
    else
    {
      throw new Exception('" . $modelNameInTitleCase . " not found', 404);
    }
  }
  
  public function create()
  {
    \$sql = \"INSERT INTO " . $modelNameInLowerCase . "s(
              " . $createFields . "
            ) VALUES (" . $interrogation . ")\";
    
    \$queryResult = self::query(
      \$sql, 
      '" . $bindParamsLetters . "', 
      [
        " . $paramsForCreateStatement . "
      ]
    );

    \$this->" . $this->primaryKey . " = \$queryResult->insert_id;

    return true;
  }

  public function update()
  {
    \$sql = \"UPDATE " . $modelNameInLowerCase . "s  
            SET " . $setParams . " 
            WHERE " . $this->primaryKey . " = ?\";
    
    \$queryResult = self::query(
      \$sql, 
      '" . $bindParamsLetters . "i', 
      [
        " . $paramsForCreateStatement . ",
        \$this->" . $this->primaryKey . "
      ]
    )->result;
  }

  " . $getters . "

  " . $setters . "
  
  protected static function getTableName() { return '" . $modelNameInLowerCase . "s'; }
  
  protected static function getPrimaryKeyName() { return '" . $this->primaryKey . "'; }
}
?>";

    return $result;
  }

  private function generatePropsOfClass()
  {
    $propsOfClass = '';

    foreach($this->props as $key => $val)
    {
      $propsOfClass .= "protected \${$key};\n" . $this->getTabs(1);
    }

    return $propsOfClass;
  }

  private function generatePropsValidationRules()
  {
    $propsValidationRules = '';

    foreach($this->props as $key => $val)
    {
      $propsValidationRules .= "'{$key}' => '" . $this->getRules($val) . "',\n" . $this->getTabs(3);
    }

    return substr($propsValidationRules, 0, -8);
  }

  private function generateFieldsAsStringsArray()
  {
    $stringArray = '';

    foreach($this->props as $key => $val)
    {
      $stringArray .= "'{$key}',\n" . $this->getTabs(5);
    }

    return substr($stringArray, 0, -12);
  }

  private function generateFieldsAsStringsArrayForCreateStament()
  {
    $stringArray = '';

    foreach($this->props as $key => $val)
    {
      $stringArray .= "{$key},\n" . $this->getTabs(7);
    }

    return substr($stringArray, 0, -16);
  }

  private function generateInterrogationMarksForCreateStatement()
  {
    $marks = $this->repeatText("?, ", count($this->props));

    return substr($marks, 0, -2);
  }

  private function generateBindParamLetters()
  {
    $bindParamsLetters = '';

    foreach($this->props as $key => $val)
    {
      $bindParamsLetters .= $val === 'b' ? 'i' : $val;
    }

    return $bindParamsLetters;
  }

  private function generateParamsForCreateStatement()
  {
    $bindParams = '';

    foreach($this->props as $key => $val)
    {
      $bindParams .= "\$this->{$key},\n" . $this->getTabs(4);    
    }

    return substr($bindParams, 0, -10);
  }

  private function generateSetParams()
  {
    $setParams = '';

    foreach($this->props as $key => $val)
    {
      $setParams .= "{$key} = ?,\n" . $this->getTabs(8);
    }

    return substr($setParams, 0, -18);
  }

  private function generateGetters()
  {
    $getters = '';

    foreach($this->props as $key => $val)
    {
      $getters .= "public function get" . $this->getSetName($key) . "(){ return \$this->{$key}; }\n\n" . $this->getTabs(1);
    }

    return substr($getters, 0, -4);
  }

  private function generateSetters()
  {
    $setters = '';

    foreach($this->props as $key => $val)
    {
      $setters .= "public function set" . $this->getSetName($key) . "(" . $this->letterTypeToType($val) . " \${$key})\n" . $this->getTabs(1) .
                  "{\n" . $this->getTabs(2) .
                  "self::validateOneField('{$key}', \${$key});\n" . $this->getTabs(2) .
                  "\$this->{$key} = \${$key};\n" .
        $this->getTabs(1) . "}\n\n" . $this->getTabs(1);
    }

    return substr($setters, 0, -4);
  }
}
?>