CREATE DATABASE todo;

USE todo;

CREATE TABLE activities (
  activityId INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(250) NOT NULL,
  comment VARCHAR(1000) DEFAULT NULL,
  isCompleted BOOL DEFAULT 0,
  createdDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  finishedDate TIMESTAMP NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO activities(title, comment)
VALUES ('Do homework', 'Data structures homework'),
       ('Take out the trash', NULL);